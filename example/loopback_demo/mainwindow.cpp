#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
    , is_capture_(false)
    , video_capture_engine_(new VideoCaptureEngine())
{
    ui->setupUi(this);


    m_pLocalPreview = new QWidget(this);
    m_pRemotePreview = new QWidget(this);
    m_pLocalPreview->setStyleSheet("border: 1px solid black");
    m_pRemotePreview->setStyleSheet("border: 1px solid black");

    ResizePreview();

    OpenVideo();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent* event) 
{
    if (event->type() != QEvent::WindowStateChange)
        return;

    ResizePreview();
}

void MainWindow::ResizePreview() 
{
    int localWidth = width() / 4;
    int localHeight = height() / 4;
    int localX = width() - localWidth;
    int localY = height() - localHeight;
    m_pLocalPreview->setGeometry(QRect(localX - 5, localY - 5, localWidth, localHeight));
    m_pRemotePreview->setGeometry(QRect(0, 0, width(), height()));
}

void MainWindow::OpenVideo() {
    if (is_capture_) {
        return;
    }
    if (video_devices_.empty()) {
        video_devices_ = video_capture_engine_->EnumVideoDevices();
    }
    if (video_devices_.empty()) {
        return;
    }
    if (current_device_id_.empty()) {
        current_device_id_ = video_devices_[0].device_id;
    }
    if (!video_frame_observer_) {
        video_frame_observer_.reset(new VideoFrameObserver());
        video_frame_observer_->SetObserver(this);
    }
    video_capture_engine_->RegisteVideoFrameObserver(video_frame_observer_);
    VideoProfile video_profile;
    video_profile.width = 1280;
    video_profile.height = 720;
    video_profile.fps = 30;
    video_capture_engine_->SetVideoProfile(video_profile);
    is_capture_ = true;
    video_capture_engine_->StartCapture(current_device_id_);
}

void MainWindow::CloseVideo() {
    if (is_capture_) {
        video_capture_engine_->StopCapture();
        is_capture_ = false;
    }
}


void MainWindow::OnFrame(std::shared_ptr<VideoFrame> video_frame) 
{
}
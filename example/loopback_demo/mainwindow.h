#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QEvent>
#include <mutex>
#include <queue>

#include "video_capture_engine.h"
#include "video_common.h"
//#include "video_device_choose_window.h"
#include "video_device_event_handler.h"
#include "video_render.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void OnFrame(std::shared_ptr<VideoFrame> video_frame);

protected:
    void changeEvent(QEvent* event);

private:
    void OpenVideo();
    void CloseVideo();
    void ResizePreview();
    Ui::MainWindow *ui;
    QWidget* m_pLocalPreview;
    QWidget* m_pRemotePreview;

    std::shared_ptr<VideoRender> video_render_;
    std::shared_ptr<VideoFrameObserver> video_frame_observer_;
    //std::shared_ptr<VideoDeviceWindow> video_device_window_;
    std::shared_ptr<VideoCaptureEngine> video_capture_engine_;

    bool is_capture_;
    std::vector<VideoDeviceInfo> video_devices_;
    std::string current_device_id_{};
    HWND video_device_hwnd_{};

    std::mutex mtx_;
    std::queue<std::shared_ptr<VideoFrame>> frame_queue_;
    std::thread render_thread_;
    bool running_{};
    std::condition_variable cv_;
};
#endif // MAINWINDOW_H
